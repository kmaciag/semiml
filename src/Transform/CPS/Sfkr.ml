open Lang.CPS.Ast
open Lang.CPS
open Common.Primop.Repr
exception Not_reduce

let unwrap_label value = match value with
  | Var(l) | Label(l) -> l
  | _ -> failwith ("Sfkr::unwrap_label -- application of non function")

let transform program = 
let rec trans expr = 
    { expr with e_kind = match expr.e_kind with 
  | Record(vs, x, e) -> Record(vs, x, trans e)
  | Select(i, v, x, e) -> 
    let rec findFetch v k =
      let rec findFetch' vv path = 
        match path with
        | Offp 0 -> vv
        | Offp k -> 
          (match MetaData.CPS.RecordInfo.try_get (unwrap_label v) with
            | Some(vs) -> 
            (match List.nth vs k with
              | (v', Offp 0) -> v'
              | (v', path) -> findFetch' v' path
            )
            | _ ->  raise Not_reduce
            )
        | Selp(k, path') -> 
          let f = findFetch vv k in findFetch' f path'
    in
        match MetaData.CPS.RecordInfo.try_get (unwrap_label v) with
        | Some(vs) -> 
          (match List.nth vs k with
            | (v', Offp 0) ->  v'
            | (v', path) -> findFetch' v' path
           )
        | _ -> raise Not_reduce 
      in
        (try  let v' =  findFetch  v i in
        let e' = trans e in
            (BetaContraction.replace_vars ([(x, v')],[]) e').e_kind
        with
        | _ -> Select(i, v, x, trans e))
  | Offset(i, v, x, e) -> Offset(i, v, x, trans e)
  | App(f, xs) -> expr.e_kind
  | Fix(decls, e) -> 
    let reduce_body = fun (fname, vs, body) -> (fname, vs,  trans body) in
    Fix(List.map reduce_body decls, trans e)
  | Switch(v, es) -> Switch(v, List.map trans es)
  | Primop(op, vs, xs, es) -> Primop(op, vs, xs, List.map trans es)
  } 
in trans program 

let c_sfkr = Contract.create
  ~description: "Selection from known record"
  ~languages: [Language.CPS] 
  "transform:sfkr"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name: "CPS:sfkr"
    ~require:
      [ Lang.CPS.Contracts.right_scopes
        ; Analysis.CPS.RecordInfo.contract
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; Analysis.CPS.RecordInfo.contract
      ; c_sfkr
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ; Contract.saves_contract Analysis.CPS.RecordInfo.contract
      ]
    transform