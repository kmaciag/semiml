
type node =
| Node : 'lang Types.language * Contract.Set.t -> node

type command =
| Cmd_Analyse   of Analysis.t
| Cmd_Eval      of Evaluator.t
| Cmd_Pretty    of Pretty.t
| Cmd_Transform of Transform.t

type path = command list

module Node : sig
  type t = node
  val compare  : t -> t -> int
  val equal    : t -> t -> bool
  val instance : t -> t -> bool
end
