
val get     : Common.Tag.t -> Common.Var.Set.t
val try_get : Common.Tag.t -> Common.Var.Set.t option

val set : Common.Tag.t -> Common.Var.Set.t -> unit
