open Lang.CPS.Ast

type info = RECInfo of (value * accesspath) list

let rec analyse expr =
   match expr.e_kind with
  | Record(vs, x, e) ->  MetaData.CPS.RecordInfo.set x vs; analyse e
  | Select(i, v, x, e) -> analyse e
  | Offset(i, v, x, e) -> analyse e
  | Fix(decls, e) ->
    let analyse_body = fun (fname, vs, body) -> analyse body in
       List.iter analyse_body decls; analyse e
  | Switch(v, es) -> List.iter analyse es
  | Primop(op, vs, xs, es) -> List.iter analyse es
  | _ -> ()

let clean_analyse expr =
  MetaData.CPS.RecordInfo.reset();
  analyse expr

let contract = Contract.create
  ~languages: [Language.CPS]
  "analyse:recordinfo"

let register () =
  Compiler.register_analysis
    ~lang:      Compiler.Lang_CPS
    ~name:      "CPS:recordinfo"
    ~require:   [ ]
    ~contracts: [ contract ]
    analyse