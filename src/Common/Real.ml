
type t = float

let is_boxed () = true

let add x y = x +. y
let sub x y = x -. y
let mul x y = x *. y
let div x y = x /. y

let equal     x y = x = y
let not_equal x y = x <> y

let lt x y = x < y
let le x y = x <= y
let gt x y = x > y
let ge x y = x >= y

let of_sml_string str =
  float_of_string (String.init 
    (String.length str)
    (fun i -> if str.[i] = '~' then '-' else str.[i]))

let minus_regexp   = Str.regexp "-"
let plus_regexp    = Str.regexp "\\+"
let dot_end_regexp = Str.regexp "\\.$"

let to_sml_string x =
  string_of_float x
  |> Str.global_replace minus_regexp   "~"
  |> Str.global_replace plus_regexp    ""
  |> Str.global_replace dot_end_regexp ".0"
