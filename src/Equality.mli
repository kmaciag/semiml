
type (_, _) t =
| Eq  : ('a, 'a) t
| Neq : ('a, 'b) t

type (_, _) ord =
| Ord_Lt : ('a, 'b) ord
| Ord_Eq : ('a, 'a) ord
| Ord_Gt : ('a, 'b) ord
