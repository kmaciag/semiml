open Lang.CPS.Ast

type arity = BOT
			| UNK
		  | TOP
			| COUNT of int *bool

type fninfo =  {arity : arity list ref;
			  	alias : var option ref;
			  	escape : bool ref}

type info = FNinfo of fninfo
			| ARGinfo of int ref
			| RECinfo of int

val get     : var -> info

val try_get : var -> info option

val set     : var -> info -> unit
val reset : unit -> unit
