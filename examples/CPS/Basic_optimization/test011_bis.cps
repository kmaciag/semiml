fix factorial(k,n) =
  fix d(z) =
    primop phneq(z, 0)
    =>call k(1)
    =>fix k2(v) =
        let r = primop mul(v, n)
        call k(r)
      let m = primop sub(n, 1)
      call factorial(k2, m)
    end
  primop ile(n,0)
  => call d(1)
  => call d(0)
  end
call factorial(top_cont, 5)