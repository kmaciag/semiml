
open Lang.NuL.Ast
open Errors

let c_constant_fold = Contract.create "transform:constant_fold"

let rec transform expr =
  match expr.e_kind with
  | (Succ | Var _ | Num _) -> expr
  | Abs(x, body) ->
    { expr with e_kind = Abs(x, transform body) }
  | App(sak, k) when sak.e_kind = Succ ->
    (match k.e_kind with
      | Num n ->
        { expr with e_kind = Num(n + 1) }
      | Abs(x, body) ->
         warning ~tag:expr.e_tag "succ of function doesn't make sense";
         expr
      | _ -> let k' = transform k in
        { expr with e_kind = App(sak, k') }
    )
  | App(e1, e2) ->
    let e1' = transform e1 in
    let e2' = transform e2 in
    { e_tag = expr.e_tag; e_kind = App(e1', e2') }
  | Case(e1, (x2, e2), e3, (x4, e4)) ->
    { expr with e_kind =
      Case(transform e1, (x2, transform e2), transform e3, (x4, transform e4))
    }

let register () =
   Compiler.register_transformation
    ~source: Compiler.Lang_NuL
    ~target: Compiler.Lang_NuL
    ~name:   "NuL:constant_fold"
    ~require:
      [ Lang.NuL.Contracts.unique_tags
      ; Analysis.NuL.FreeVars.contract
      ]
    ~contracts:
      [ Lang.NuL.Contracts.unique_tags
      ; c_constant_fold
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.NuL.Contracts.right_scopes
      ; Contract.saves_contract Lang.NuL.Contracts.unique_vars
      ]
    transform

