open Lang.CPS.Ast

val get     : var -> (expr * var list)
val try_get : var -> (expr * var list) option

val set     : var -> (expr * var list) -> unit
val reset   : unit -> unit
