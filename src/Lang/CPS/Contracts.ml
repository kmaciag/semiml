open Language

let right_scopes = Common.Contracts.right_scopes
let unique_vars  = Common.Contracts.unique_vars
let unique_tags  = Common.Contracts.unique_tags

let primop_arity = Contract.create
  ~description:
    "Every primop operation has right number of arguments, \
    binds right number of variables and has right number of continuations."
  ~languages: [CPS]
  "contract:primop_arity"

let closure_conversion = Contract.create
  ~description: "Transform program from CPS to closure passing style"
  ~languages: [CPS]
  "transform:closure_conversion"

let lift_definitions = Contract.create
  ~description: "List definitions of funtions to top level fix"
  ~languages: [CPS]
  "contract:lift_definitions"

let registers_spilled = Contract.create
  ~description:
    "No expression requires more than N live variables"
  ~languages: [CPS]
  "contract:registers_spilled"
