fix factorial(k, n) =
  primop ile(n, 0)
  =>call k(1)
  =>fix k2(v) =
      let r = primop mul(v, n)
      call k(r)
    let m = primop sub(n, 1)
    call factorial(k2, m)
  end
let r = record
  factorial
call top_cont(r)
