let table = Hashtbl.create 1024

let get tag =
  try Hashtbl.find table tag with
  | Not_found -> 0

let set tag value = Hashtbl.add table tag value
