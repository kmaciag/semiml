val inlined_function: Lang.CPS.Ast.expr -> Lang.CPS.Ast.expr_kind

val replace_vars : (Lang.CPS.Ast.var * Lang.CPS.Ast.value) list *
           (Lang.CPS.Ast.var * Lang.CPS.Ast.var) list->Lang.CPS.Ast.expr -> Lang.CPS.Ast.expr
val transform: Lang.CPS.Ast.expr -> Lang.CPS.Ast.expr
val c_beta_contraction : Contract.t

val register : unit -> unit