fix d(nn) =
  fix f(k,x) =
    let x0 = #0 x
    let x1 = #1 x
    let x2 = #2 x
    call f0(k,x0,x1,x2)
  and f0(k,x,x0,x1) =
    let x2 = record
      x
      x0
      x1
    let a = #0 x2
    let b = #1 x2
    let q = #2 x2
    let c = primop mul(a,b)
    call k(c)
  let r = record
    7
    42
    4
  let x = #0 r
  let x0 = #1 r
  let x1 = #2 r
  call f0(nn,x,x0,x1)
call d(top_cont)