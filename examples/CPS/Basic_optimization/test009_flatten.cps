fix d(nn) =
  fix f(k,x) =
    let a = #0 x
    let b = #1 x
    let q = #2 x
    let c = primop mul(a,b)
    call k(c)
  let r = record
   	7
    42
    4
  call f(nn,r)
call d(top_cont)