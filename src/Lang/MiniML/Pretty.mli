
val pretty_type : 
  Type.TVarScope.t * Type.TConScope.t ->
  int ->
  Type.t ->
    Printing.Box.t
