let table = Hashtbl.create 1024

let try_get vr =
  try Hashtbl.find table vr; true with
  | Not_found -> false

let set v  =
  Hashtbl.replace table v ()

let reset () = 
  Hashtbl.reset table
