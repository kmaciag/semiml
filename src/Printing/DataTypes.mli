
type color =
| Black
| Red
| Green
| Yellow
| Blue
| Magenta
| Cyan
| White

type base_attribute =
| BA_Bold
| BA_Underline
| BA_Blink
| BA_Negative
| BA_CrossedOut
| BA_FgColor of color
| BA_BgColor of color

type attribute =
| Error
| Keyword
| Number
| Literal
| Constant
| Constructor
| Operator
| Paren
| Variable
| Base of base_attribute

type box =
| B_Text      of attribute list * string
| B_Prefix    of box * box
| B_Suffix    of box * box
| B_Indent    of int * box
| B_WhiteSep  of box
| B_BreakLine of box
| B_Box       of box list
