open Lang.CPS.Ast


let printFreeVars set =
  Common.Var.Set.iter (fun x -> print_string (Common.Var.string_of_var x)) set

let find args arg = 
  List.fold_left  (fun a -> fun x -> a || x == arg)  false args

let unwrap_label value = match value with
  | Var(l) | Label(l) -> l
  | _ -> failwith ("EtaReduction::unwrap_label -- application of non function")

let transform program = 
let rec trans expr =  
    { expr with e_kind = match expr.e_kind with 
  | Record(vs, x, e) -> Record(vs, x, trans e)
  | Select(i, v, x, e) -> Select(i, v, x, trans e)
  | Offset(i, v, x, e) -> Offset(i, v, x, trans e)
  | App(f, xs) -> expr.e_kind
  | Fix(decls, e) ->
    let e' = ref e in
      let reduce = fun (fname, formals, body) ->
        match body.e_kind with
        | App(fname', args)-> 
              begin
              if not (find formals fname) && not (find formals (unwrap_label fname')) then
                begin
                  e' := BetaContraction.replace_vars ([(fname,fname')],[]) !e'; false
                end
              else true
            end
        | _ -> true
      in let decls' =  List.filter reduce decls
    in Fix(decls', trans !e') 
  | Switch(v, es) -> Switch(v, List.map trans es)
  | Primop(p, vs, xs, es) -> Primop(p, vs, xs, List.map trans es)
  }
in  trans program


let c_eta_reduction = Contract.create 
  ~description: " Eta reduction"
  ~languages: [Language.CPS]
  "transform:etareduction"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name: "CPS:etareduction"
    ~require:
      [ Lang.CPS.Contracts.right_scopes
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; c_eta_reduction
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ]
    transform